from st3m.application import Application, ApplicationContext
from st3m.input import InputController
from st3m.ui.view import View
import bl00mbox
import leds
import random

# 1m = 5pixel
VIEW_SCALE = 5
MAX_SPEED = 100
STATION_LENGTH = 210
TRAIN_LENGTH = 191
SIFA_COUNTDOWN = 10 * 1000

SIFA_NORMAL = 0
SIFA_WARN = 1
SIFA_STOP = 2

TRAIN_PATH = "/flash/sys/apps/train-sim/assets/train.png"
TREE_PATH = "/flash/sys/apps/train-sim/assets/tree.png"

SAMPLE_STOERUNG = "/sys/apps/train-sim/assets/stoerung.wav"
SAMPLE_SIFA = "/sys/apps/train-sim/assets/sifa.wav"
SAMPLE_SIFA_ZB = "/sys/apps/train-sim/assets/sifa-zb.wav"

# Game states
STATE_DRIVING = 0
STATE_BOARDING = 1
STATE_GAMEOVER = 2


class TrainSim(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.is_loading = True

        self.input = InputController()
        self.channel = bl00mbox.Channel("Train Simulator")
        self.samples = {}

    def _reset(self) -> None:
        self._time = random.randint(400 * 60, 1080 * 60) * 1000
        self._state = STATE_DRIVING

        self._speed = 60
        self._trees = []
        self._last_station = None
        self._next_station = None
        self._sifa_countdown = None
        self._sifa_tapped = False
        self._last_sifa_audio = 0
        self._last_stoerung = None

        self._boarding_time_start = None
        self._boarding_time_end = None

        self._gameover_time_end = None

        self._view_pos = 0
        self._start_driving_level()

    def _start_driving_level(self) -> None:
        if self._next_station:
            self._last_station = (self._next_station[0] - self._view_pos, self._next_station[1])

        distance = random.randint(200, 800) * VIEW_SCALE
        schedule_time_s = distance / 20 / VIEW_SCALE  # Estimate time
        if not self._last_station or self._last_station[1] > self._time:
            last_time = self._time
        else:
            last_time = self._last_station[1]

        self._next_station = (
            distance,
            last_time + (schedule_time_s + random.randint(10, 15)) * 1000,
        )

        self._trees = []
        pos = 240 if self._last_station else 0
        while pos < self._next_station[0] - 160:
            pos += random.randint(40, 140)
            is_front = random.random() > 0.5
            self._trees.append((pos, is_front))

        self._view_pos = 0
        self._sifa_countdown = SIFA_COUNTDOWN
        self._state = STATE_DRIVING

    def _start_boarding(self) -> None:
        self._boarding_time_start = self._time
        self._boarding_time_end = self._time + random.randint(5, 10) * 1000
        self._state = STATE_BOARDING

    def _show_gameover(self) -> None:
        self._gameover_time_end = self._time + 3000
        self._state = STATE_GAMEOVER

    def draw(self, ctx: Context) -> None:
        if self.is_loading:
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 30
            ctx.move_to(0, 0)
            ctx.rgb(1, 1, 1).text("Loading")
            return

        if self._state == STATE_GAMEOVER:
            ctx.rgb(1, 0, 0).rectangle(-120, -120, 240, 240).fill()
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 42
            ctx.move_to(0, 0)
            ctx.rgb(1, 1, 1).text("GAME OVER")
            return

        ctx.rgb(191/255, 235/255, 255/255).rectangle(-120, -120, 240, 134).fill()
        ctx.rgb(81/255, 172/255, 36/255).rectangle(-120, 14, 240, 106).fill()

        for tree in self._trees:
            if not tree[1] and self._view_pos - 25 <= tree[0] <= self._view_pos + 240:
                ctx.image(TREE_PATH, -120 + tree[0] - self._view_pos, -41, -1, -1)

        ctx.image(TRAIN_PATH, -96, -20, -1, -1)

        for tree in self._trees:
            if tree[1] and self._view_pos - 25 <= tree[0] <= self._view_pos + 240:
                ctx.image(TREE_PATH, -120 + tree[0] - self._view_pos, -38, -1, -1)

        ctx.move_to(-120, 14).line_to(120, 14).rgb(0, 0, 0).stroke()

        if self._last_station and self._view_pos < self._last_station[0] + STATION_LENGTH:
            ctx.rgb(50/255, 50/255, 50/255)\
               .rectangle(-120 + self._last_station[0] - self._view_pos, 12, 210, 6)\
               .fill()

        if self._next_station and self._view_pos + 240 > self._next_station[0]:
            ctx.rgb(50/255, 50/255, 50/255)\
               .rectangle(-120 + self._next_station[0] - self._view_pos, 12, 210, 6)\
               .fill()

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 22
        ctx.move_to(0, 80)
        ctx.rgb(1, 1, 1).text(str(int(self._speed)) + " km/h")

        ctx.font_size = 14
        if self._state == STATE_DRIVING:
            ctx.move_to(0, 45)
            ctx.rgb(1, 1, 1).text("Next stop in:")
            ctx.move_to(0, 60)
            ctx.rgb(1, 1, 1).text(str(int((self._next_station[0] - self._view_pos) / VIEW_SCALE)) + " m")
            ctx.move_to(0, 105)
            if self._sifa_countdown < 0:
                ctx.rgb(1, 0, 0).text("SIFA")
            else:
                ctx.rgb(1, 1, 1).text(str(int(self._sifa_countdown // 1000) + 1))
        elif self._state == STATE_BOARDING:
            ctx.move_to(0, 45)
            ctx.rgb(1, 1, 1).text("Boarding ...")
            ctx.rgba(1, 1, 1, 0.5).rectangle(-80, 55, 160, 10).fill()
            ctx.rgb(1, 1, 1).rectangle(-80, 55, 160 - int((self._boarding_time_end - self._time) / (self._boarding_time_end - self._boarding_time_start) * 160), 10).fill()


        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 50).fill()
        ctx.move_to(0, -109)
        ctx.rgb(0.7, 0.7, 0.7).text("{:0>2}:{:0>2}:{:0>2}".format(
                int((self._time / 60000) // 60),
                int((self._time / 60000) % 60),
                int((self._time / 1000) % 60)
            )
        )
        if self._next_station:
            ctx.move_to(0, -94)
            ctx.rgb(0.7, 0.7, 1.0).text("expected arr.: {:0>2}:{:0>2}:{:0>2}".format(
                    int((self._next_station[1] / 60000) // 60),
                    int((self._next_station[1] / 60000) % 60),
                    int((self._next_station[1] / 1000) % 60)
                )
            ) 

            ctx.move_to(0, -79)
            if self._next_station[1] > self._time - 60 * 1000:
                ctx.rgb(0.7, 1.0, 0.7).text("on schedule :)")
            else:
                delay = self._time - self._next_station[1]
                ctx.rgb(1.0, 0.3, 0.3).text("1 mins delayed!".format(
                        int((delay / 60000) % 60),
                    )
                ) 

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.is_loading and len(self.samples) == 0:
            leds.set_all_rgb(0, 0, 0)
            leds.set_rgb(4, 1, 0, 0)
            leds.update()
            sample = self.channel.new(bl00mbox.patches.sampler, SAMPLE_STOERUNG)
            sample.signals.output = self.channel.mixer
            self.samples["stoerung"] = sample
            leds.set_rgb(5, 1, 0, 0)
            leds.update()
            sample = self.channel.new(bl00mbox.patches.sampler, SAMPLE_SIFA)
            sample.signals.output = self.channel.mixer
            self.samples["sifa"] = sample
            leds.set_rgb(6, 1, 0, 0)
            leds.update()
            sample = self.channel.new(bl00mbox.patches.sampler, SAMPLE_SIFA_ZB)
            sample.signals.output = self.channel.mixer
            self.samples["sifa-zb"] = sample
            leds.set_rgb(7, 1, 0, 0)
            leds.update()
            return
        if self.is_loading and len(self.samples) > 0:
            self.is_loading = False
            leds.set_all_rgb(0, 0, 0)
            leds.update()
            self._reset()
            return

        self.input.think(ins, delta_ms)

        self._time += delta_ms

        #if self._last_stoerung is None or self._time - self._last_stoerung > 5000:
        #    self._last_stoerung = self._time
        #    self.samples["stoerung"].signals.trigger.start()

        if self._state == STATE_DRIVING:
            acc_pressed = self.input.buttons.app.right.down
            brk_pressed = self.input.buttons.app.left.down
            #acc_pressed = self.input.captouch.petals[2].pressure > 0
            #brk_pressed = self.input.captouch.petals[3].pressure > 0
            if acc_pressed:
                self._speed += delta_ms / 100
                self._speed = min(MAX_SPEED, self._speed)
            elif brk_pressed:
                self._speed -= delta_ms / 100
                self._speed = max(0, self._speed)

            pix_per_s = self._speed * VIEW_SCALE / 3.6
            self._view_pos += (pix_per_s * delta_ms) / 1000

            if self._speed == 0 and abs(self._view_pos - self._next_station[0]) < 5 * VIEW_SCALE:
                self._start_boarding()

            if self._speed > 0 and self._view_pos > self._next_station[0] + 5 * VIEW_SCALE:
                self._show_gameover()

            # SIFA resetting
            sifa_pressed = self.input.captouch.petals[3].pressure > 0
            if sifa_pressed and not self._sifa_tapped:
                self._sifa_tapped = True
                self._sifa_countdown = SIFA_COUNTDOWN
                self._sifa_mode = SIFA_NORMAL
            elif not sifa_pressed:
                self._sifa_tapped = False

            # SIFA status
            self._sifa_countdown -= delta_ms
            if self._sifa_countdown < 0:
                self._sifa_mode = SIFA_STOP
                self._speed -= delta_ms / 90
                self._speed = max(0, self._speed)
                if self._time - self._last_sifa_audio > 2000 and self._speed > 0:
                    self.samples["sifa-zb"].signals.trigger.start()
                    self._last_sifa_audio = self._time
            elif self._sifa_countdown < 5 * 1000:
                self._sifa_mode = SIFA_WARN
                if self._time - self._last_sifa_audio > 1300 and self._speed > 0:
                    self.samples["sifa"].signals.trigger.start()
                    self._last_sifa_audio = self._time

        elif self._state == STATE_BOARDING:
            if self._time > self._boarding_time_end:
                self._start_driving_level()

        elif self._state == STATE_GAMEOVER:
            if self._time > self._gameover_time_end:
                self._reset()


if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(TrainSim(ApplicationContext()))
